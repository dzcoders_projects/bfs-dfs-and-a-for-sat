import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by dzcod3r on 3/4/17.
 */
public class MainLargeur {

    public static void main(String[] args) {
        int nombreFile = 10;
        String fileName="";
        SAT sat= null;

        HashSet<Integer> interpretation;
        long debutTraitement=-1;
        double finTraitement=-1;
        FileWriter outputSat= null;
        FileWriter outputDead= null;
        FileWriter outputExpt=null;

        try {
            outputSat = new FileWriter("largNonSat.txt", true);
            outputExpt = new FileWriter("largNonException.txt", true);
            outputDead= new FileWriter("largeNonDead.txt", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 1; i <= nombreFile; i++) {
            fileName= "unsatfiles/uuf75-0"+i+".cnf";

            try {

                sat = new SAT(fileName, 75);
                //traitement
                debutTraitement= System.currentTimeMillis();
                interpretation= sat.executerRechercheLargeur();
                finTraitement= (System.currentTimeMillis()-debutTraitement)* 0.001;

                //sauvegarde:
                //fileName tempsDExecution nbLittereux interpretation
                if(!sat.getStrategieLargeur().isTraitementFini()){
                    //dead line ici on met un autre fichier pour garder les max fitness
                    outputDead.write(fileName+"\t"+finTraitement+"\t"+sat.getStrategieLargeur().getFitnessMax()+"\n");
                    System.out.println(fileName+"\t"+finTraitement+"\t"+sat.getStrategieLargeur().getFitnessMax()+"\n");
                }else{

                    outputSat.write(fileName+"\t"+finTraitement+"\t"+interpretation.size()+"\n");
                    System.out.println(fileName+"\t"+finTraitement+"\t"+interpretation.size());
                }

            } catch (OutOfMemoryError e) {
                finTraitement= (System.currentTimeMillis()-debutTraitement)* 0.001;

                try {

                    outputExpt.write(fileName+"\t"+finTraitement+"\t"+sat.getStrategieLargeur().getFitnessMax()+"\n");
                    System.out.println(fileName+"\t"+finTraitement+"\t"+sat.getStrategieLargeur().getFitnessMax()+"\n");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            sat=null;
        }
        try {
            outputDead.close();
            outputExpt.close();
            outputSat.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
