import java.io.IOException;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by dzcod3r on 3/4/17.
 */
public class SAT {
    private CNF cnf;
    private RechercheProfondeur strategieProfondeur;
    private RechercheLargeur strategieLargeur;
    private RechercheAEtoile strategieAEtoile;
    private int profondeurLimite;

    public int getProfondeurLimite() {
        return profondeurLimite;
    }

    public void setProfondeurLimite(int profondeurLimite) {
        this.profondeurLimite = profondeurLimite;
    }

    public SAT(String chemin, int profondeurLimite) throws IOException {
        this.profondeurLimite = profondeurLimite;
        strategieProfondeur = new RechercheProfondeur();
        strategieLargeur = new RechercheLargeur();
        strategieAEtoile = new RechercheAEtoile();
        strategieProfondeur.setSat(this);
        strategieAEtoile.setSat(this);
        strategieLargeur.setSat(this);
        cnf = new CNF(chemin);
    }
    HashSet<Integer> executerRechercheProfondeur(){
        return strategieProfondeur.rechercher();
    }
    HashSet<Integer> executerRechercheLargeur(){
        return strategieLargeur.rechercher();
    }
    HashSet<Integer> executerRechercheAEtoile(){
        return strategieAEtoile.rechercher();
    }

    //setters + getters

    public CNF getCnf() {
        return cnf;
    }

    public void setCnf(CNF cnf) {
        this.cnf = cnf;
    }

    public RechercheProfondeur getStrategieProfondeur() {
        return strategieProfondeur;
    }

    public void setStrategieProfondeur(RechercheProfondeur strategieProfondeur) {
        this.strategieProfondeur = strategieProfondeur;
    }


    public RechercheLargeur getStrategieLargeur() {
        return strategieLargeur;
    }


    public RechercheAEtoile getStrategieAEtoile() {
        return strategieAEtoile;
    }

    @Override
    public String toString() {
        return "SAT{" +
                "cnf=" + cnf +
                '}';
    }
}
