import java.util.*;

/**
 * Created by dzcod3r on 3/4/17.
 */
public class RechercheProfondeur {
    private Stack<Etat> open;
    private SAT sat;
    private int fitnessMax=0;
    private boolean traitementFini= true;

    HashSet<Integer> rechercher(){
        double debutTraitement= System.currentTimeMillis();
        int deadLine=15*60; // variable de control de deadLine 15 min
        int fitness = 0,profondeur=0;
        int nombreClauses,profondeurLimite;
        nombreClauses = sat.getCnf().getNombreClauses();
        profondeurLimite =sat.getProfondeurLimite();
        boolean notFound = true;
        Etat etatCourant=null;
        HashSet<Integer> interpretation = new HashSet<>();

        ArrayList<Etat> fils= developper(null);

        //empiler dans ouverts
        for (Etat e : fils) {
            open.push(e);
        }
        while (notFound && !open.isEmpty() && traitementFini){
            //depiler etat
            etatCourant = open.pop();
            interpretation = etatCourant.getInterpretation();

            profondeur = interpretation.size();
                fitness=fitness(interpretation);
                if(fitness>fitnessMax){
                    fitnessMax=fitness;
                    System.out.println("fit: "+ fitnessMax);
                }
                if(fitness == nombreClauses){
                    notFound = false;
                }
                else{
                    if(profondeur != profondeurLimite){
                        fils = developper(etatCourant);
                        //empiler dans ouverts
                        for (Etat e : fils) {
                            open.push(e);
                        }
                    }
                }
            if ((((System.currentTimeMillis()-debutTraitement)* 0.001) > deadLine)){
                traitementFini=false;
            }
        }
        if(notFound){
            return new HashSet<>();
        }
        return interpretation;
    }

//    public TreeSet<Etat> developper(Etat parent) {
//        HashSet<Clause> clauses = sat.getCnf().getClauses();
//        int nbVariable = sat.getCnf().getNombreVariable();
//        int taille = nbVariable * 2;
//        HashSet<Integer> interpretation;
//        int[] frequence = new int[taille];
//        boolean containAscendent;
//        if(parent != null){
//            interpretation = parent.getInterpretation();
//        }
//        else{
//            interpretation = new HashSet<>();
//        }
//        //parcourir les clauses:
//        for (Clause clause : clauses) {
//            int cpt = 0;
//            containAscendent= false;
//            //verifier si la clause est satifiable
//            for(int i: interpretation){
//                if(clause.contains(i)){
//                    containAscendent=true;
//                    break;
//                }
//                else if(clause.contains(-i)){
//                    cpt++;
//                }
//            }
//            //
//            if(cpt == clause.getLitteraux().size()){
//                return new TreeSet<>();
//            }
//            //si elle n'est pas satisfiable on calcule la fréquence des litteraux
//            if (!containAscendent) {
//                HashSet<Integer> litteraux = clause.getLitteraux();
//                for (int litteral : litteraux){
//                    //on incremente
//                    if (litteral > 0) {
//                        frequence[litteral - 1] += 1;
//                    } else if (litteral < 0) {
//                        frequence[Math.abs(litteral) + taille / 2 - 1] += 1;
//                    }
//                }
//            }
//        }
//
//        //creation des etats
//        int indiceMax=0;
//
//        TreeSet<Etat> etats = new TreeSet<>();
//        for (int litteral = 0; litteral < nbVariable; litteral++) {
//            if (frequence[litteral] > 0 && !interpretation.contains(-(litteral+1))) {
//                if(frequence[indiceMax]< frequence[litteral]){
//                    indiceMax=litteral;
//                }
//            }
//        }
//        for (int litteral = nbVariable; litteral < taille; litteral++) {
//            if (frequence[litteral] > 0 && !interpretation.contains(litteral + 1 - taille / 2)) {
//                if(frequence[indiceMax] < frequence[litteral]){
//                    indiceMax=litteral;
//                }
//            }
//        }
//        if(frequence[indiceMax]>0){
//
//            if(indiceMax < nbVariable){
//                if(frequence[indiceMax+nbVariable]>0){
//                    etats.add(new Etat(-(indiceMax + 1), frequence[indiceMax + nbVariable], parent));
//                }
//                etats.add(new Etat(indiceMax+1,frequence[indiceMax],parent));
//            }
//            else{
//                if(frequence[indiceMax - nbVariable]>0 ){
//                    etats.add(new Etat(indiceMax - nbVariable +1, frequence[indiceMax - nbVariable], parent));
//                }
//                etats.add(new Etat(-(indiceMax - nbVariable+1),frequence[indiceMax],parent));
//            }
//            return etats;
//        }
//        else{
//            return new TreeSet<>();
//        }
//    }


    //setters +getters



    public int  fitness(HashSet<Integer> interpretation) {
        HashSet<Clause> clauses = sat.getCnf().getClauses();
        int nbclauses = 0;
        for (Clause clause : clauses) {
            for (int i : interpretation) {
                if (clause.contains(i)) {
                    nbclauses++;
                    break;
                }
            }
        }
        return nbclauses;

    }

    ArrayList<Etat> developper(Etat parent){
        int nbvariables= sat.getCnf().getNombreVariable();
        ArrayList<Etat> etats = new ArrayList<>();
        HashSet<Integer> interpretation;
        int rand;
        if(parent != null){
            interpretation = parent.getInterpretation();
        }
        else{
            interpretation = new HashSet<>();
        }

        do{
            rand = (int) (Math.random()*100)%nbvariables +1;
        }while(interpretation.contains(rand) || interpretation.contains(-rand));
        etats.add(new Etat(rand, parent));
        etats.add(new Etat(-rand, parent));

        return etats;


    }

    public RechercheProfondeur() {
        open = new Stack<>();
    }

    public Stack<Etat> getOpen() {
        return open;
    }

    public void setOpen(Stack<Etat> open) {
        this.open = open;
    }

    public boolean isTraitementFini() {
        return traitementFini;
    }

    public void setTraitementFini(boolean traitementFini) {
        this.traitementFini = traitementFini;
    }

    public SAT getSat() {
        return sat;
    }

    public void setSat(SAT sat) {
        this.sat = sat;
    }

    public int getFitnessMax() {
        return fitnessMax;
    }

    public void setFitnessMax(int fitnessMax) {
        this.fitnessMax = fitnessMax;
    }
}
