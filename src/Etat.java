import java.util.HashSet;

/**
 * Created by chayma on 3/2/17.
 */
public class Etat implements Comparable<Etat>{

    private int valeur, frequence, fitness;
    private Etat etatParent;

    HashSet<Integer> getInterpretation(){
        Etat parent=this;
        HashSet<Integer> interpretation=new HashSet<>();
        while(parent!=null){
            interpretation.add(parent.getValeur());
            parent=parent.getEtatParent();
        }
        return interpretation;
    }

    int getFitness(){
        return fitness;
    }
    //à tester

    //getters + setters

    public Etat(int valeur, Etat parent){
        this.valeur=valeur;
        this.etatParent= parent;
    }

    public Etat(int valeur, int fitness, Etat etatParent) {
        this.valeur = valeur;
        this.fitness = fitness;
        this.etatParent = etatParent;
    }


    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public int getFrequence() {
        return frequence;
    }

    public void setFrequence(int frequence) {
        this.frequence = frequence;
    }

    public Etat getEtatParent() {
        return etatParent;
    }

    public void setEtatParent(Etat etatParent) {
        this.etatParent = etatParent;
    }

    @Override
    public String toString() {
        return "val: "+valeur+"\tfreq: "+frequence+"\n"+"\tfitness: "+fitness;
    }

    @Override
    public int compareTo(Etat etat) {
        if(valeur == etat.valeur)return 0;
        else if(fitness> etat.fitness) return 1;
        else return -1;
    }
}
