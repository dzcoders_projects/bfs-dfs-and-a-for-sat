import java.util.HashSet;

/**
 * Created by dzcod3r on 3/4/17.
 */
public class Clause {
    private HashSet<Integer> litteraux;

    //setters +getters
    public Clause(HashSet<Integer> litteraux) {
        this.litteraux = litteraux;
    }

    public HashSet<Integer> getLitteraux() {
        return litteraux;
    }

    public void setLitteraux(HashSet<Integer> litteraux) {
        this.litteraux = litteraux;
    }

    @Override
    public String toString() {
        return "Clause{" +
                "litteraux=" + litteraux +
                '}'+"\n";
    }

    public boolean contains(int i) {
        return  litteraux.contains(i);
    }
}
