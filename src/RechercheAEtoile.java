import java.util.*;

/**
 * Created by dzcod3r on 3/4/17.
 */
public class RechercheAEtoile {
    private HashSet<Etat> open;
    private SAT sat;
    private boolean traitementFini= true;
    private int fitnessMax;
    HashSet<Integer> rechercher(){
        //timer
        double debutTraitement= System.currentTimeMillis();
        int deadLine=15*60; // variable de control de deadLine 15 min
        int fitness = 0,profondeur=0;
        int nombreClauses,profondeurLimite;
        nombreClauses = sat.getCnf().getNombreClauses();
        profondeurLimite =sat.getProfondeurLimite();
        boolean notFound = true;
        Etat etatCourant=null;
        HashSet<Integer> interpretation = new HashSet<>();

        HashSet<Etat> fils= developper(null);

        //empiler dans ouverts
        open.addAll(fils);
        int fitnessTemp =Collections.min(open).getFitness();
        fitnessMax = (nombreClauses-(fitnessTemp-profondeur));
        while (notFound && !open.isEmpty() && traitementFini ){
            //chercher le min;
            etatCourant= Collections.min(open);
            //suprimer l'etat etat qui a la max fitness;
            open.remove(etatCourant);
            interpretation = etatCourant.getInterpretation();
            profondeur = interpretation.size();

                fitness=etatCourant.getFitness();
                if(fitness<fitnessTemp){
                    fitnessTemp=fitness;
                }
                if(fitnessMax<(nombreClauses-(fitness-profondeur))){
                    fitnessMax=(nombreClauses-(fitness-profondeur));
                }
                if(fitness == profondeur){
                    notFound = false;
                }
                else{
                    if(profondeur != profondeurLimite){
                        fils = developper(etatCourant);
                        //empiler dans ouverts
                        open.addAll(fils);
                    }
                }
                if ((((System.currentTimeMillis()-debutTraitement)* 0.001) > deadLine)){
                    traitementFini=false;
                }

        }
        if(notFound){
            return new HashSet<>();
        }
        return interpretation;
    }

    private HashSet<Etat> developper(Etat parent) {
        HashSet<Clause> clauses = sat.getCnf().getClauses();
        int nbVariable = sat.getCnf().getNombreVariable();
        int taille = nbVariable * 2;
        HashSet<Integer> interpretation;
        int[] frequence = new int[taille];
        boolean containAscendent;
        if(parent != null){
            interpretation = parent.getInterpretation();
        }
        else{
            interpretation = new HashSet<>();
        }
        //parcourir les clauses:
        for (Clause clause : clauses) {
            int cpt = 0;
            containAscendent= false;
            //verifier si la clause est satifiable
            for(int i: interpretation){
                if(clause.contains(i)){
                    containAscendent=true;
                    break;
                }
                else if(clause.contains(-i)){
                    cpt++;
                }
            }
            //
            if(cpt == clause.getLitteraux().size()){
                return new HashSet<>();
            }
            //si elle n'est pas satisfiable on calcule la fréquence des litteraux
            if (!containAscendent) {
                HashSet<Integer> litteraux = clause.getLitteraux();
                for (int litteral : litteraux){
                    //on incremente
                    if (litteral > 0) {
                        frequence[litteral - 1] += 1;
                    } else if (litteral < 0) {
                        frequence[Math.abs(litteral) + taille / 2 - 1] += 1;
                    }
                }
            }
        }
        int nbclauseInsatisfiableParent;
        if(parent== null){
            nbclauseInsatisfiableParent = clauses.size() ;
        }
        else{
            nbclauseInsatisfiableParent = parent.getFitness();
        }

        //creation des etats
        int indiceMax=-1;

        HashSet<Etat> etats = new HashSet<>();
        for (int litteral = 0; litteral < nbVariable; litteral++) {
            if (frequence[litteral] > 0 && !interpretation.contains(-(litteral+1))) {
                if((indiceMax== -1) || (frequence[indiceMax]< frequence[litteral])){
                    indiceMax=litteral;
                }
            }
        }
        for (int litteral = nbVariable; litteral < taille; litteral++) {
            if (frequence[litteral] > 0 && !interpretation.contains(litteral + 1 - taille / 2)) {
                if((indiceMax== -1) || (frequence[indiceMax] < frequence[litteral])){
                    indiceMax=litteral;
                }
            }
        }
        if((indiceMax != -1) && (frequence[indiceMax]>0)){
            int clausesInsat;
            if(indiceMax < nbVariable){
                if(frequence[indiceMax+nbVariable]>0){
                    clausesInsat= nbclauseInsatisfiableParent - frequence[indiceMax + nbVariable] ;
                    etats.add(new Etat(-(indiceMax + 1), clausesInsat + 1, parent));
                }
                clausesInsat= nbclauseInsatisfiableParent - frequence[indiceMax] ;
                etats.add(new Etat(indiceMax+1,clausesInsat +1,parent));
            }
            else{
                if(frequence[indiceMax - nbVariable]>0 ){
                    clausesInsat= nbclauseInsatisfiableParent -  frequence[indiceMax - nbVariable];
                    etats.add(new Etat(indiceMax - nbVariable +1, clausesInsat + 1, parent));
                }
                clausesInsat=  nbclauseInsatisfiableParent - frequence[indiceMax];
                etats.add(new Etat(-(indiceMax - nbVariable+1),clausesInsat + 1,parent));
            }
            return etats;
        }
        else{
            return new HashSet<>();
        }
    }


    //setters +getters

    public RechercheAEtoile() {
        open = new HashSet<>();
    }

    public HashSet<Etat> getOpen() {
        return open;
    }

    public void setOpen(HashSet<Etat> open) {
        this.open = open;
    }

    public boolean isTraitementFini() {
        return traitementFini;
    }

    public void setTraitementFini(boolean traitementFini) {
        this.traitementFini = traitementFini;
    }

    public SAT getSat() {
        return sat;
    }

    public void setSat(SAT sat) {
        this.sat = sat;
    }

    public int getFitnessMax() {
        return fitnessMax;
    }
}
